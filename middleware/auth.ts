import {useUser} from "~/composables/use-user";

export default defineNuxtRouteMiddleware((to, from) => {

    const {isAdmin, user} = useUser()

    if(!isAdmin.value){
        return navigateTo("/")
    }


})