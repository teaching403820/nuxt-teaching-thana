export default defineNuxtPlugin(() => {


    return {
        provide: {

            secret: 'this is my secret!',

            sayHello: () => {
                return 'hello world'
            }

        }
    }

})