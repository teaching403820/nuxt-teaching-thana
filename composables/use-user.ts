import {use} from "h3";


export const useUser = () => {

    const user = useState('state-user', () => {
        return {
            username: 'kumar',
            email: 'kumar@hello.com',
            type: 'USER'
        }
    })


    const setUserDetails = (username: string, email: string, type: 'ADMIN' | 'USER') => {
        user.value.username = username;
        user.value.email = email;
        user.value.type = type;
    };


    const isAdmin = computed(() => {
        return user.value.type === 'ADMIN'
    })

    return {
        user,
        setUserDetails,
        isAdmin,
    }

};