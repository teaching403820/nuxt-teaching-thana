import type {Ref} from "vue";


export const useUppercase = () => {


    const input = reactive({
        text: ''
    })

    const upperCasedInput = computed(() => {
        return input.text.toUpperCase();
    })


    return {
        upperCasedInput,
        input
    }

}